package com.github.format00.coinsprice.di.module

import com.github.format00.coinsprice.api.CoinMarketCapService
import com.github.format00.coinsprice.app.orders_screen.*
import com.github.format00.coinsprice.di.qualifier.UiThread
import com.github.format00.coinsprice.di.qualifier.WorkingThread
import com.github.format00.coinsprice.mapper.OrderMapper
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import nl.qbusict.cupboard.DatabaseCompartment


@Module
class OrdersModule(private val view: OrdersView) {

    @Provides
    fun provideOrdersMapper() : OrderMapper = OrderMapper()

    @Provides
    fun provideOrdersView(): OrdersView = view

    @Provides
    fun provideOrdersRepository(cupboard: DatabaseCompartment, service: CoinMarketCapService,
                                mapper : OrderMapper): OrdersRepository
            = OrdersRepositoryImpl(cupboard, service, mapper)

    @Provides
    fun provideOrdersPresenter(view: OrdersView, repo: OrdersRepository, @WorkingThread workingThread: Scheduler,
                               @UiThread uiThread: Scheduler): OrdersPresenter = OrdersPresenterImpl(repo, view, workingThread, uiThread)
}