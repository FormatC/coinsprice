package com.github.format00.coinsprice.di.component

import com.github.format00.coinsprice.app.coins_screen.PricesFragment
import com.github.format00.coinsprice.di.module.AppModule
import com.github.format00.coinsprice.di.module.DataModule
import com.github.format00.coinsprice.di.module.NetworkModule
import com.github.format00.coinsprice.di.module.PricesModule
import dagger.Component

@Component(modules = arrayOf(PricesModule::class, AppModule::class, NetworkModule::class, DataModule::class))
interface PricesComponent {
    fun inject(fragment : PricesFragment)
}