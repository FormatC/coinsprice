package com.github.format00.coinsprice.mapper

import com.github.format00.coinsprice.model.Order
import com.github.format00.coinsprice.model.entity.OrderEntity

class OrderMapper {

    fun map(entity: OrderEntity): Order {
        return Order(entity.coinId, entity.coinName, entity.coinSymbol, entity.coinPrice, entity.totalCoinsAmount, entity.totalUsdSpent, null)
    }

    fun map(order : Order) : OrderEntity {
        return OrderEntity(order.coinId, order.coinName, order.coinSymbol, order.coinPrice, order.totalCoinsAmount, order.totalUsdSpent)
    }

}