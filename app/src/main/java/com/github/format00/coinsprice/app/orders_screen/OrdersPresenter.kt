package com.github.format00.coinsprice.app.orders_screen

import com.github.format00.coinsprice.model.Order

interface OrdersPresenter {
    fun loadOrders()
    fun deleteOrder(order : Order, position : Int)
    fun loadTotalSpentUsd()
    fun retrieveTotalSpentValue(list : List<Order>) : Double
}