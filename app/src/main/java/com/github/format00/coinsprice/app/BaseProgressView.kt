package com.github.format00.coinsprice.app

interface BaseProgressView<in T> : BaseView<T> {
    fun showProgress()
    fun hideProgress()
}