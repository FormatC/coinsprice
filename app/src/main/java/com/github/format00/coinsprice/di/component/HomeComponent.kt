package com.github.format00.coinsprice.di.component

import com.github.format00.coinsprice.app.home.HomeActivity
import com.github.format00.coinsprice.di.module.AppModule
import com.github.format00.coinsprice.di.module.HomeModule
import dagger.Component

@Component(modules = arrayOf(HomeModule::class, AppModule::class))
interface HomeComponent {
    fun inject(activity : HomeActivity)
}