package com.github.format00.coinsprice.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.github.format00.coinsprice.model.entity.CoinEntity;
import com.github.format00.coinsprice.model.entity.OrderEntity;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class OrdersSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "orders.db";
    private static final int DATABASE_VERSION = 4;

    static {
        cupboard().register(OrderEntity.class);
        cupboard().register(CoinEntity.class);
    }

    public OrdersSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        cupboard().withDatabase(db).createTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        cupboard().withDatabase(db).upgradeTables();
    }
}