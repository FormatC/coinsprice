package com.github.format00.coinsprice.app.home

class HomePresenterImpl(private val repository: HomeRepository) : HomePresenter {

    override fun saveBalance(balance: Double) {
        repository.saveBalance(balance)
    }

    override fun retrieveBalance(): Double {
        return repository.retrieveBalance()
    }
}