package com.github.format00.coinsprice.model

import com.google.gson.annotations.SerializedName

data class Coin(@SerializedName("id") var id: String, @SerializedName("name") var name: String,
                @SerializedName("symbol") var symbol: String, @SerializedName("price_usd") var priceUsd: String,
                @SerializedName("percent_change_1h") var hourChange: String?,
                @SerializedName("percent_change_24h") var dayChange: String?,
                @SerializedName("percent_change_7d") var weekChange: String?)