package com.github.format00.coinsprice.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.github.format00.coinsprice.R
import com.github.format00.coinsprice.model.Coin
import com.github.format00.coinsprice.model.Order
import java.text.DecimalFormat


class OrdersAdapter(private val context: Context, ordersList: MutableList<Order>, private val listener : OrderLongClickListener)
    : BaseAdapter<Order, OrdersAdapter.CoinsViewHolder>(ordersList) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinsViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_orders, parent, false)
        return CoinsViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CoinsViewHolder, position: Int) {
        val order = data[position]

        val currentPrice = order.coin?.priceUsd?.toDouble() ?: return
        val profit = currentPrice - order.coinPrice
        val totalChange = (currentPrice * 100 / order.coinPrice) - 100.0
        val totalProfit = (currentPrice * order.totalCoinsAmount) - (order.coinPrice * order.totalCoinsAmount)
        holder.coinName.text = order.coinName
        holder.totalCoinsAmount.text = "${formatDouble(order.totalCoinsAmount, "##.##")} ${order.coinSymbol}"
        holder.totalUsdSpent.text = "${formatDouble(order.totalUsdSpent, "##.##")}$"
        holder.boughtForValue.text = "${formatDouble(order.coinPrice, "##.#######")}$"
        holder.profitValue.text = "${formatDouble(profit, "##.#")}$"
        holder.currentPrice.text = "1 ${order.coinSymbol} = ${formatDouble(currentPrice, "##.#######")}$"

        setupWeekChange(holder, order.coin)
        setupDayChange(holder, order.coin)
        setupHourChange(holder, order.coin)
        setupTotalProfit(holder, totalChange, totalProfit)
        setupTextView(holder.profitValue)

        holder.itemView.setOnLongClickListener {
            listener.onOrderLongClick(order, position)
            false
        }
    }

    private fun formatDouble(number : Double, pattern : String) : String  = DecimalFormat(pattern).format(number)

    private fun setupWeekChange(holder: CoinsViewHolder, currentItem: Coin?) {
        if (currentItem?.weekChange == null) {
            holder.weekChangeValue.visibility = View.GONE
            holder.weekChangeLabel.visibility = View.GONE
        } else {
            holder.weekChangeValue.visibility = View.VISIBLE
            holder.weekChangeLabel.visibility = View.VISIBLE
            holder.weekChangeValue.text = "${currentItem.weekChange}%"
            setupTextView(holder.weekChangeValue)
        }
    }

    private fun setupDayChange(holder: CoinsViewHolder, currentItem: Coin?) {
        if (currentItem?.dayChange == null) {
            holder.dayChangeValue.visibility = View.GONE
            holder.dayChangeLabel.visibility = View.GONE
        } else {
            holder.dayChangeValue.visibility = View.VISIBLE
            holder.dayChangeLabel.visibility = View.VISIBLE
            holder.dayChangeValue.text = "${currentItem.dayChange}%"
            setupTextView(holder.dayChangeValue)
        }
    }


    private fun setupHourChange(holder: CoinsViewHolder, currentItem: Coin?) {
        if (currentItem?.hourChange == null) {
            holder.hourChangeValue.visibility = View.GONE
            holder.hourChangeLabel.visibility = View.GONE
        } else {
            holder.hourChangeValue.visibility = View.VISIBLE
            holder.hourChangeLabel.visibility = View.VISIBLE
            holder.hourChangeValue.text = "${currentItem.hourChange}%"
            setupTextView(holder.hourChangeValue)
        }
    }

    private fun setupTotalProfit(holder: CoinsViewHolder, totalChangeValue: Double, totalProfit: Double) {
        var totalChangeStr = DecimalFormat("##.#").format(totalChangeValue)
        val totalProfitStr =  DecimalFormat("##.#").format(totalProfit)
        if (!totalChangeStr.startsWith("-")) {
            totalChangeStr = "+$totalChangeStr"
        }

        holder.totalProfitUsdValue.text = "$totalProfitStr$ ($totalChangeStr%)"
        setupTextView(holder.totalProfitUsdValue)
    }

    private fun setupTextView(textView: TextView?) {
        val text = textView?.text
        if (text?.startsWith('-')!!) {
            textView.setTextColor(context.resources.getColor(R.color.red))
        } else {
            textView.setTextColor(context.resources.getColor(R.color.green))
            textView.text = "+$text"
        }
    }

    class CoinsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {



        @BindView(R.id.totalCoinsAmount)
        lateinit var totalCoinsAmount: TextView

        @BindView(R.id.totalUsdSpent)
        lateinit var totalUsdSpent: TextView

        @BindView(R.id.boughtForValue)
        lateinit var boughtForValue: TextView

        @BindView(R.id.profitValue)
        lateinit var profitValue: TextView

        @BindView(R.id.coinName)
        lateinit var coinName: TextView

        @BindView(R.id.hourChangeValue)
        lateinit var hourChangeValue: TextView

        @BindView(R.id.dayChangeValue)
        lateinit var dayChangeValue: TextView

        @BindView(R.id.weekChangeValue)
        lateinit var weekChangeValue: TextView

        @BindView(R.id.weekChangeLabel)
        lateinit var weekChangeLabel: TextView

        @BindView(R.id.dayChangeLabel)
        lateinit var dayChangeLabel: TextView

        @BindView(R.id.hourChangeLabel)
        lateinit var hourChangeLabel: TextView

        @BindView(R.id.currentPrice)
        lateinit var currentPrice: TextView

        @BindView(R.id.totalProfitUsdValue)
        lateinit var totalProfitUsdValue: TextView

        init {
            ButterKnife.bind(this, itemView)
        }
    }

    interface OrderLongClickListener {
        fun onOrderLongClick(order : Order, position : Int)
    }
}
