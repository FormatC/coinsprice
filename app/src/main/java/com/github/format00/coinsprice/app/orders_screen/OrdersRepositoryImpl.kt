package com.github.format00.coinsprice.app.orders_screen

import android.util.Log
import com.github.format00.coinsprice.api.CoinMarketCapService
import com.github.format00.coinsprice.mapper.OrderMapper
import com.github.format00.coinsprice.model.Order
import com.github.format00.coinsprice.model.entity.OrderEntity
import io.reactivex.Observable
import nl.qbusict.cupboard.DatabaseCompartment

class OrdersRepositoryImpl(private val cupboard: DatabaseCompartment, private val service: CoinMarketCapService,
                           private val mapper : OrderMapper) : OrdersRepository {

    val LOG_TAG: String = OrdersRepositoryImpl::class.java.name
    override fun retrieveOrders(): Observable<List<Order>> {
        return Observable.just(cupboard.query(OrderEntity::class.java)
                .list().mapOrders())
    }

    override fun retrieveFullOrders(): Observable<List<Order>> {
        val ordersWithoutCoins = retrieveOrders()
        return ordersWithoutCoins.map {
            it.map {
                val body = service.getCoinInfoCall(it.coinId).execute().body()
                val coin = body?.get(0)
                it.coin = coin
                Log.d(LOG_TAG, "Loaded ${it.coin?.id}; old price = ${it.coinPrice}, new price = ${coin?.priceUsd}")
                it
            }
        }
    }

    override fun deleteOrder(order: Order): Observable<Boolean> {
        return Observable.just(cupboard.delete(order.toEntity()))
    }

    fun Order.toEntity() : OrderEntity = mapper.map(this)

    fun List<OrderEntity>.mapOrders() : List<Order> = this.map { mapper.map(it) }.toList()
}