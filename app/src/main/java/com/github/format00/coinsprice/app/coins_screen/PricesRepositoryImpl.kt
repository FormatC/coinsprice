package com.github.format00.coinsprice.app.coins_screen

import android.content.Context
import android.content.SharedPreferences
import com.github.format00.coinsprice.Const
import com.github.format00.coinsprice.model.entity.OrderEntity
import nl.qbusict.cupboard.DatabaseCompartment

class PricesRepositoryImpl(context : Context, private val cupboard: DatabaseCompartment) : PricesRepository {
    private val prefs : SharedPreferences = context.getSharedPreferences(Const.HOME_PREFERENCES_TAG, Context.MODE_PRIVATE)

    override fun retrieveBalance(): Double {
        val balanceStr = prefs.getString(Const.BALANCE_TAG, "0.00")
        return balanceStr.toDouble()
    }

    override fun saveOrder(order: OrderEntity) {
        val id = cupboard.put(order)
    }

    override fun updateCurrentBalance(newBalance: Double) {
        prefs.edit().putString(Const.BALANCE_TAG, newBalance.toString()).commit()
    }

}