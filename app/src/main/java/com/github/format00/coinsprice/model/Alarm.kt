package com.github.format00.coinsprice.model

data class Alarm(var coinName : String, var coinId : String, var alarmPositive : Double,
                 var alarmNegative : Double, var sellPositive : Double, var sellNegative : Double,
                 var coin : Coin?)

