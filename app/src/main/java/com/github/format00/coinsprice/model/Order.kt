package com.github.format00.coinsprice.model

data class Order(var coinId: String, var coinName: String, var coinSymbol: String, var coinPrice: Double,
                 var totalCoinsAmount: Double, var totalUsdSpent: Double, var coin: Coin?)