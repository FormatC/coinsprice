package com.github.format00.coinsprice.di.module

import com.github.format00.coinsprice.Const
import com.github.format00.coinsprice.api.CoinMarketCapService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    fun provideRetorit(): Retrofit {
        val okHttpClientBuilder = OkHttpClient.Builder()
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)

        val okHttpClient = okHttpClientBuilder.build()
        val retrofitBuilder = Retrofit.Builder()
                .baseUrl(Const.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
        return retrofitBuilder.build()
    }

    @Provides
    fun provideCoinMarketCapService(retrofit: Retrofit): CoinMarketCapService = retrofit.create(CoinMarketCapService::class.java)

}