package com.github.format00.coinsprice.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.github.format00.coinsprice.R
import com.github.format00.coinsprice.model.Coin


class PricesAdapter(private val context: Context, coinList: MutableList<Coin>,
                    private val listener : PriceClickListener)
    : BaseAdapter<Coin, PricesAdapter.PriceViewHolder>(coinList) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PriceViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_prices, parent, false)
        return PriceViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PriceViewHolder, position: Int) {
        val currentItem = data[position]
        holder.name.text = currentItem.name
        holder.dayChange.text = currentItem.dayChange + "%"
        holder.currentPrice.text = currentItem.priceUsd + "$"

        setupWeekChange(holder, currentItem)
        setupDayChange(holder, currentItem)

        holder.itemView.setOnClickListener {
            listener.onPriceItemClicked(currentItem)
        }
    }

    private fun setupWeekChange(holder : PriceViewHolder, currentItem : Coin) {
        if (currentItem.weekChange == null) {
            holder.weekChange.visibility = View.GONE
            holder.weekLabel.visibility = View.GONE
        } else {
            holder.weekChange.visibility = View.VISIBLE
            holder.weekLabel.visibility = View.VISIBLE
            holder.weekChange.text = currentItem.weekChange + "%"
            setupTextView(holder.weekChange)
        }
    }

    private fun setupDayChange(holder : PriceViewHolder, currentItem : Coin) {
        if (currentItem.dayChange == null) {
            holder.dayChange.visibility = View.GONE
            holder.dayLabel.visibility = View.GONE
        } else {
            holder.dayChange.visibility = View.VISIBLE
            holder.dayLabel.visibility = View.VISIBLE
            holder.dayChange.text = currentItem.dayChange + "%"
            setupTextView(holder.dayChange)
        }
    }

    private fun setupTextView(textView: TextView?) {
        val text = textView?.text
        if (text?.startsWith('-')!!) {
            textView.setTextColor(context.resources.getColor(R.color.red))
        } else {
            textView.setTextColor(context.resources.getColor(R.color.green))
            textView.text = "+" + text
        }
    }

    class PriceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.dayValue)
        lateinit var dayChange: TextView

        @BindView(R.id.weekValue)
        lateinit var weekChange: TextView

        @BindView(R.id.currentPrice)
        lateinit var currentPrice: TextView

        @BindView(R.id.weekLabel)
        lateinit var weekLabel: TextView

        @BindView(R.id.name)
        lateinit var name: TextView

        @BindView(R.id.dayLabel)
        lateinit var dayLabel: TextView

        init {
            ButterKnife.bind(this, itemView)
        }
    }

    interface PriceClickListener {
        fun onPriceItemClicked(coin: Coin)
    }
}
