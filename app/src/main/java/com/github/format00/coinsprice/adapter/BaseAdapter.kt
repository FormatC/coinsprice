package com.github.format00.coinsprice.adapter

import android.support.v7.widget.RecyclerView

abstract class BaseAdapter<T, VH : RecyclerView.ViewHolder>(sourceData : List<T>)
    : RecyclerView.Adapter<VH>() {

    protected var data : MutableList<T> = ArrayList(sourceData)

    fun getItems(): List<T> = ArrayList(data)

    fun swap(newData: List<T>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    fun addAll(extraData: List<T>) {
        data.addAll(extraData)
        notifyDataSetChanged()
    }

    fun remove(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, data.size)
    }

    override fun getItemCount(): Int {
        return data.size
    }

}