package com.github.format00.coinsprice.app.orders_screen

import com.github.format00.coinsprice.app.BaseView
import com.github.format00.coinsprice.model.Order

interface OrdersView : BaseView<List<Order>> {
    fun showProgress(isDialog : Boolean)
    fun hideProgress(isDialog : Boolean)
    fun removeAdapterItem(position : Int)
}