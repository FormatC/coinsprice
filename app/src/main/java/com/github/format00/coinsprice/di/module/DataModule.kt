package com.github.format00.coinsprice.di.module

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.github.format00.coinsprice.db.OrdersSQLiteOpenHelper
import dagger.Module
import dagger.Provides
import nl.qbusict.cupboard.CupboardFactory
import nl.qbusict.cupboard.DatabaseCompartment

@Module
class DataModule {

    @Provides
    fun provideSQLiteOpenHelper(context: Context): SQLiteOpenHelper = OrdersSQLiteOpenHelper(context)

    @Provides
    fun provideDatabase(helper: SQLiteOpenHelper): SQLiteDatabase = helper.writableDatabase

    @Provides
    fun provideCupboard(db: SQLiteDatabase): DatabaseCompartment = CupboardFactory.cupboard().withDatabase(db)
}