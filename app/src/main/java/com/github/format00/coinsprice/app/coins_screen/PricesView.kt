package com.github.format00.coinsprice.app.coins_screen

import com.github.format00.coinsprice.app.BaseProgressView
import com.github.format00.coinsprice.model.Coin

interface PricesView : BaseProgressView<List<Coin>>  {
}