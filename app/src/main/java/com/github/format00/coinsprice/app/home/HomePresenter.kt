package com.github.format00.coinsprice.app.home

interface HomePresenter {
    fun saveBalance(balance : Double)
    fun retrieveBalance() : Double
}