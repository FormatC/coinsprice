package com.github.format00.coinsprice.model.entity

class AlarmEntity() {
    var _id : Long? = null
    var coinName : String = ""
    var coinId : String = ""
    var alarmPositive : Double = 0.0
    var alarmNegative : Double = 0.0
    var sellPositive : Double = 0.0
    var sellNegative : Double = 0.0

    constructor(coinName: String, coinId: String, alarmPositive: Double, alarmNegative: Double,
                sellPositive: Double, sellNegative: Double) : this() {
        this.coinName = coinName
        this.coinId = coinId
        this.alarmPositive = alarmPositive
        this.alarmNegative = alarmNegative
        this.sellPositive = sellPositive
        this.sellNegative = sellNegative
    }

    override fun toString(): String {
        return "AlarmEntity(_id=$_id, coinName='$coinName', coinId='$coinId', alarmPositive=$alarmPositive," +
                " alarmNegative=$alarmNegative, sellPositive=$sellPositive, sellNegative=$sellNegative)"
    }


}