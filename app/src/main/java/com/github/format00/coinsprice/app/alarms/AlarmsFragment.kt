package com.github.format00.coinsprice.app.alarms

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.github.format00.coinsprice.R
import com.github.format00.coinsprice.adapter.AlarmsAdapter

class AlarmsFragment : Fragment() {

    @BindView(R.id.alarms)
    lateinit var alarmsRecyclerView : RecyclerView

    private lateinit var adapter : AlarmsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_alarms, container, false)
        ButterKnife.bind(this, view)

        adapter = AlarmsAdapter(ArrayList())
        alarmsRecyclerView.adapter = adapter
        alarmsRecyclerView.layoutManager = LinearLayoutManager(activity)

        return view
    }

}