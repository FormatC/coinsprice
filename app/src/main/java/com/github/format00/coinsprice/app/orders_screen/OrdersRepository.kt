package com.github.format00.coinsprice.app.orders_screen

import com.github.format00.coinsprice.model.Order
import io.reactivex.Observable

interface OrdersRepository {
    fun retrieveOrders() : Observable<List<Order>>
    fun retrieveFullOrders() : Observable<List<Order>>
    fun deleteOrder(order : Order) : Observable<Boolean>
}