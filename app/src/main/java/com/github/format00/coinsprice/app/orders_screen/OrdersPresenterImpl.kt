package com.github.format00.coinsprice.app.orders_screen

import com.github.format00.coinsprice.model.Order
import io.reactivex.Scheduler

class OrdersPresenterImpl(private val repository: OrdersRepository, private val view: OrdersView,
                          private val workingScheduler: Scheduler, private val uiScheduler: Scheduler) : OrdersPresenter {

    override fun retrieveTotalSpentValue(list: List<Order>) : Double {
        var totalValue : Double = 0.0
        list.forEach { totalValue += it.totalUsdSpent }
        return totalValue
    }

    override fun loadOrders() {
        view.showProgress(false)
        repository.retrieveFullOrders()
                .subscribeOn(workingScheduler)
                .observeOn(uiScheduler)
                .subscribe(this::onSuccessLoading, this::onLoadingFail)
    }

    private fun onSuccessLoading(data: List<Order>) {
        view.hideProgress(false)
        view.onSuccessLoading(data)
    }

    private fun onLoadingFail(t: Throwable) {
        view.hideProgress(false)
        view.onLoadingFail(t)
    }

    override fun deleteOrder(order: Order, position : Int) {
        view.showProgress(true)
        repository.deleteOrder(order)
                .filter { it }
                .subscribeOn(workingScheduler)
                .observeOn(uiScheduler)
                .subscribe({ view.hideProgress(true); view.removeAdapterItem(position)}, { view.hideProgress(true) })
    }

    override fun loadTotalSpentUsd() {

    }

}