package com.github.format00.coinsprice.app.home

import android.content.Context
import android.content.SharedPreferences
import com.github.format00.coinsprice.Const

class HomeRepositoryImpl(private val context : Context) : HomeRepository  {

    private val prefs : SharedPreferences = context.getSharedPreferences(Const.HOME_PREFERENCES_TAG, Context.MODE_PRIVATE)

    override fun saveBalance(balance: Double) {
        prefs.edit().putString(Const.BALANCE_TAG, balance.toString()).commit()
    }

    override fun retrieveBalance(): Double {
        val balanceStr = prefs.getString(Const.BALANCE_TAG, "0.00")
        return balanceStr.toDouble()
    }
}