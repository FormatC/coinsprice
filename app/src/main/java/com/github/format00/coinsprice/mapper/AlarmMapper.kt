package com.github.format00.coinsprice.mapper

import com.github.format00.coinsprice.model.Alarm
import com.github.format00.coinsprice.model.entity.AlarmEntity

class AlarmMapper {

    fun map(alarm: Alarm): AlarmEntity {
        return AlarmEntity(alarm.coinName, alarm.coinId, alarm.alarmPositive, alarm.alarmNegative,
                alarm.sellPositive, alarm.sellNegative)
    }

    fun map(entity : AlarmEntity) : Alarm {
        return Alarm(entity.coinName, entity.coinId, entity.alarmPositive, entity.alarmNegative,
                entity.sellPositive, entity.sellNegative, null)
    }
}