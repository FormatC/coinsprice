package com.github.format00.coinsprice.util

import android.content.Context
import android.net.TrafficStats
import android.os.Process
import android.os.Vibrator
import android.util.Log

class AndroidUtils {
    companion object {
        fun vibrate(context : Context) {
            val pattern = longArrayOf(0, 100)
            val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibrator.vibrate(pattern, -1)
        }

        fun logNetworkTraffic(logTag : String) {
            var myUid = Process.myUid()
            var bytes = TrafficStats.getUidRxBytes(myUid)
            Log.d(logTag, "myUid: $myUid, KB = ${bytes / 1000}")
        }
    }
}