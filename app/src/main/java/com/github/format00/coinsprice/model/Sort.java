package com.github.format00.coinsprice.model;

public enum Sort {
    DEFAULT, PRICE_ASC, PRICE_DESC, CHANGE_24h_ASC, CHANGE_1W_ASC,
    CHANGE_24h_DESC, CHANGE_1W_DESC
}
