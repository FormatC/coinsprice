package com.github.format00.coinsprice.app.service

import android.app.Notification
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log

class DataService : Service() {

    val TAG = 777

    override fun onBind(intent: Intent): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onCreate() {
        Log.d("DATA_SERVICE", "Started")
        val builder = Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_dialer)
        val notification: Notification

        notification = builder.build()
        startForeground(TAG, notification)
    }


}
