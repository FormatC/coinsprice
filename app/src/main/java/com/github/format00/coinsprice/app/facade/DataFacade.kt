package com.github.format00.coinsprice.app.facade

interface DataFacade {
    fun startUpdating(delaySecs : Long)
    fun stopUpdating()
}