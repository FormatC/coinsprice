package com.github.format00.coinsprice.api

import com.github.format00.coinsprice.model.Coin
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface CoinMarketCapService {

    @GET("/v1/ticker")
    fun getAllPrices() : Observable<List<Coin>>

    @GET("/v1/ticker")
    fun getAllPrices(@Query("limit") limit : Int) : Observable<List<Coin>>


    @GET("/v1/ticker/{id}")
    fun getCoinInfo(@Path("id") id : String) : Observable<List<Coin>>

    @GET("/v1/ticker/{id}")
    fun getCoinInfoCall(@Path("id") id : String) : Call<List<Coin>>
}