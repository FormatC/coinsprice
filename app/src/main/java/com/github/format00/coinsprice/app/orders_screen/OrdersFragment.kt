package com.github.format00.coinsprice.app.orders_screen

import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.github.format00.coinsprice.R
import com.github.format00.coinsprice.adapter.OrdersAdapter
import com.github.format00.coinsprice.app.home.HomeActivity
import com.github.format00.coinsprice.di.component.DaggerOrdersComponent
import com.github.format00.coinsprice.di.module.AppModule
import com.github.format00.coinsprice.di.module.DataModule
import com.github.format00.coinsprice.di.module.NetworkModule
import com.github.format00.coinsprice.di.module.OrdersModule
import com.github.format00.coinsprice.model.Order
import com.github.format00.coinsprice.util.AndroidUtils
import javax.inject.Inject


class OrdersFragment : Fragment(), OrdersView {
    var LOG_TAG = OrdersFragment::class.java.name

    @BindView(R.id.orders)
    lateinit var ordersRecyclerView: RecyclerView

    @BindView(R.id.progressBar)
    lateinit var progressBar: ProgressBar

    private var ordersAdapter: OrdersAdapter? = null
    private var progressDialog: ProgressDialog? = null

    @Inject
    lateinit var presenter: OrdersPresenter

    private lateinit var totalSpentInfo: ImageView
    private var totalSpentValue: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerOrdersComponent.builder()
                .appModule(AppModule(activity))
                .networkModule(NetworkModule())
                .dataModule(DataModule())
                .ordersModule(OrdersModule(this))
                .build()
                .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(activity.applicationContext).inflate(R.layout.fragment_orders, container, false)
        ButterKnife.bind(this, view)

        totalSpentInfo = (activity as HomeActivity).totalSpentInfo
        showTotalSpentInfo()
        setupTotalSpentInfoClickListener(view)

        ordersAdapter = OrdersAdapter(activity, ArrayList<Order>(), object : OrdersAdapter.OrderLongClickListener {
            override fun onOrderLongClick(order: Order, position: Int) {
                AndroidUtils.vibrate(activity)
                val dialog = AlertDialog.Builder(activity).create()
                dialog.setTitle("Confirmation")
                dialog.setMessage("Are you sure you want to delete this order?")
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", {
                    dialog, _ ->
                    presenter.deleteOrder(order, position)
                    dialog.dismiss()
                })
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", {
                    dialog, _ ->
                    dialog.dismiss()
                })
                dialog.show()
            }
        })

        progressDialog = ProgressDialog(activity)

        val manager = LinearLayoutManager(activity)
        ordersRecyclerView.layoutManager = manager
        ordersRecyclerView.adapter = ordersAdapter

        presenter.loadOrders()
        return view
    }

    private fun setupTotalSpentInfoClickListener(container: View) {
        totalSpentInfo.setOnClickListener {
            val bar = Snackbar.make(container, "Total spent: $totalSpentValue$", Snackbar.LENGTH_INDEFINITE)
            bar.setAction("OK", { bar.dismiss() })
            bar.show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        hideTotalSpentInfo()
    }

    private fun showTotalSpentInfo() {
        totalSpentInfo.visibility = View.VISIBLE
    }

    private fun hideTotalSpentInfo() {
        totalSpentInfo.visibility = View.GONE
    }

    override fun onSuccessLoading(data: List<Order>) {
        totalSpentValue = presenter.retrieveTotalSpentValue(data)
        ordersAdapter?.swap(data)
    }

    override fun showProgress(isDialog: Boolean) {
        if (!isDialog) {
            ordersRecyclerView.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        } else {
            progressDialog?.show()
        }
    }

    override fun onLoadingFail(t: Throwable) {
        t.printStackTrace()
        Toast.makeText(context, "Error during loading data", Toast.LENGTH_SHORT).show()
    }

    override fun hideProgress(isDialog: Boolean) {
        Log.d(LOG_TAG, "hideProgress, isDialog = $isDialog")
        if (!isDialog) {
            ordersRecyclerView.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        } else {
            progressDialog?.dismiss()
        }
    }

    override fun removeAdapterItem(position: Int) {
        Log.d(LOG_TAG, "Deleted at pos: " + position)
        ordersAdapter?.remove(position)
    }

}