package com.github.format00.coinsprice.app.coins_screen

import com.github.format00.coinsprice.model.entity.OrderEntity


interface PricesRepository {
    fun saveOrder(order : OrderEntity)
    fun retrieveBalance(): Double
    fun updateCurrentBalance(newBalance : Double)
}