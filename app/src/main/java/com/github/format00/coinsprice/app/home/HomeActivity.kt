package com.github.format00.coinsprice.app.home

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import com.github.format00.coinsprice.Const
import com.github.format00.coinsprice.R
import com.github.format00.coinsprice.app.alarms.AlarmsFragment
import com.github.format00.coinsprice.app.coins_screen.PricesFragment
import com.github.format00.coinsprice.app.orders_screen.OrdersFragment
import com.github.format00.coinsprice.di.component.DaggerHomeComponent
import com.github.format00.coinsprice.di.module.AppModule
import com.github.format00.coinsprice.di.module.HomeModule
import com.github.format00.coinsprice.util.AndroidUtils
import javax.inject.Inject

open class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbarSpinner)
    lateinit var toolbarSpinner: Spinner

    @BindView(R.id.drawer_layout)
    lateinit var drawer: DrawerLayout

    @BindView(R.id.nav_view)
    lateinit var navigationView: NavigationView

    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar

    @BindView(R.id.totalSpentInfo)
    lateinit var totalSpentInfo: ImageView

    @BindView(R.id.container)
    lateinit var container : RelativeLayout

    private lateinit var toggle: ActionBarDrawerToggle

    @Inject
    lateinit var presenter: HomePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        DaggerHomeComponent.builder()
                .appModule(AppModule(this))
                .homeModule(HomeModule())
                .build()
                .inject(this)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        setupDrawer()
        switchFragment(PricesFragment(), Const.PRICES_FRAGMENT_TAG)
    }

    private fun setupDrawer() {
        toggle = object : ActionBarDrawerToggle(
                this, drawer, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            override fun onDrawerOpened(drawerView: View) {
                val balance = presenter.retrieveBalance()
                val header = navigationView.getHeaderView(0)
                val balanceTextView = header.findViewById(R.id.balanceValue) as TextView
                balanceTextView.text = balance.toString() + "$"
                super.onDrawerOpened(drawerView)
            }
        }

        drawer.setDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener(this)
        setupHeaderView(navigationView)
    }

    private fun switchFragment(fragment: Fragment, tag: String) {
        val fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction().replace(R.id.container, fragment, tag)
                .commit()
    }

    private fun setupHeaderView(navigationView: NavigationView) {
        val headerView = navigationView.getHeaderView(0)
        headerView.setOnLongClickListener {
            AndroidUtils.vibrate(this)
            setupBalanceClick()
            false
        }
    }

    private fun setupBalanceClick() {
        val view = LayoutInflater.from(this).inflate(R.layout.balance_change_dialog, null, false)
        val ok = view.findViewById(R.id.okButton) as Button
        val cancel = view.findViewById(R.id.cancelButton) as Button
        val newBalance = view.findViewById(R.id.newBalance) as EditText
        val dialog = AlertDialog.Builder(this).create()
        ok.setOnClickListener {
            val newBalanceStr = newBalance.text.toString()
            val newBalanceDouble = newBalanceStr.toDouble()
            presenter.saveBalance(newBalanceDouble)
            toggle.onDrawerOpened(drawer)
            dialog.dismiss()
        }
        cancel.setOnClickListener { dialog.dismiss() }
        dialog.setView(view)
        dialog.show()

    }

    override fun onBackPressed() {
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.base_drawer, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.nav_prices) {
            switchFragment(PricesFragment(), Const.PRICES_FRAGMENT_TAG)
        } else if (id == R.id.nav_my_coins) {
            switchFragment(OrdersFragment(), Const.COINS_FRAGMENT_TAG)
        } else if (id == R.id.nav_alarms) {
            switchFragment(AlarmsFragment(), Const.ALARMS_FRAGMENT_TAG)
        }

        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    fun getSpinner(): Spinner = toolbarSpinner

    fun hideToolbarSpinner() {
        toolbarSpinner.visibility = View.GONE
    }

    fun getTotalSpentButton() = totalSpentInfo

    fun showToolbarSpinner() {
        toolbarSpinner.visibility = View.VISIBLE
    }

}
