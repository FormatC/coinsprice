package com.github.format00.coinsprice.di.module

import android.content.Context
import com.github.format00.coinsprice.app.home.HomePresenter
import com.github.format00.coinsprice.app.home.HomePresenterImpl
import com.github.format00.coinsprice.app.home.HomeRepository
import com.github.format00.coinsprice.app.home.HomeRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class HomeModule() {

    @Provides
    fun provideHomeRepository(context: Context) : HomeRepository = HomeRepositoryImpl(context)

    @Provides
    fun provideHomePresenter(repo: HomeRepository): HomePresenter = HomePresenterImpl(repo)
}