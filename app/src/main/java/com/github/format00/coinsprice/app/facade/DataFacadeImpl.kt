package com.github.format00.coinsprice.app.facade

import com.github.format00.coinsprice.api.CoinMarketCapService
import io.reactivex.Observable
import io.reactivex.Scheduler
import nl.qbusict.cupboard.DatabaseCompartment
import java.util.concurrent.TimeUnit

class DataFacadeImpl(private val service: CoinMarketCapService, private val cupboard: DatabaseCompartment,
                     private val jobScheduler: Scheduler) : DataFacade {


    override fun startUpdating(delaySecs: Long) {
        Observable.interval(1000L, TimeUnit.MILLISECONDS)
                .flatMap { service.getAllPrices() }
                .flatMap({ Observable.fromIterable(it) })
                .doOnNext {  }
                .toList()
                .observeOn(jobScheduler)
    }

    override fun stopUpdating() {

    }

}