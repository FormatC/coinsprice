package com.github.format00.coinsprice.app.coins_screen

import com.github.format00.coinsprice.model.entity.OrderEntity
import com.github.format00.coinsprice.model.Sort

interface PricesPresenter {
    fun loadAllPrices()
    fun loadPrices(limit: Int)
    fun loadAllPricesSort(sort: Sort?)
    fun retrieveCurrentBalance(): Double
    fun updateCurrentBalance(newValue: Double)
    fun saveOrder(order: OrderEntity)
}