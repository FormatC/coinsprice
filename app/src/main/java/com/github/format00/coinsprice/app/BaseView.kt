package com.github.format00.coinsprice.app


interface BaseView<in T> {
    fun onSuccessLoading(data : T)
    fun onLoadingFail(t : Throwable)
}