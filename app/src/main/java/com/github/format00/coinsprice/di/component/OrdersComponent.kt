package com.github.format00.coinsprice.di.component

import com.github.format00.coinsprice.app.orders_screen.OrdersFragment
import com.github.format00.coinsprice.di.module.AppModule
import com.github.format00.coinsprice.di.module.DataModule
import com.github.format00.coinsprice.di.module.NetworkModule
import com.github.format00.coinsprice.di.module.OrdersModule
import dagger.Component

@Component(modules = arrayOf(OrdersModule::class, AppModule::class, NetworkModule::class, DataModule::class))
interface OrdersComponent {
    fun inject(fragment : OrdersFragment)
}