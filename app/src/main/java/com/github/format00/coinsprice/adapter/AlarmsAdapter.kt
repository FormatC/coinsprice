package com.github.format00.coinsprice.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.github.format00.coinsprice.R
import com.github.format00.coinsprice.model.Alarm

class AlarmsAdapter(data : List<Alarm>) : BaseAdapter<Alarm, AlarmsAdapter.AlarmsViewHolder>(data) {

    override fun onBindViewHolder(holder: AlarmsViewHolder, position: Int) {
        val currentItem = data[position]
        holder.alarmValueNegative.text = "-${currentItem.alarmNegative}%"
        holder.alarmValuePositive.text = "+${currentItem.alarmPositive}%"
        holder.sellValueNegative.text = "-${currentItem.sellNegative}%"
        holder.sellValuePositive.text = "-${currentItem.sellPositive}%"
        holder.coinName.text = currentItem.coinName
        holder.coinPrice.text = ""
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlarmsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_alarms, parent, false)
        return AlarmsViewHolder(view)
    }

    class AlarmsViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.coinName)
        lateinit var coinName : TextView

        @BindView(R.id.coinPrice)
        lateinit var coinPrice : TextView

        @BindView(R.id.alarmValuePositive)
        lateinit var alarmValuePositive : TextView

        @BindView(R.id.alarmValueNegative)
        lateinit var alarmValueNegative : TextView

        @BindView(R.id.sellValuePositive)
        lateinit var sellValuePositive : TextView

        @BindView(R.id.sellValueNegative)
        lateinit var sellValueNegative : TextView


        init {
            ButterKnife.bind(this, itemView)
        }
    }
}