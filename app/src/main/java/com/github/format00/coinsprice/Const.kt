package com.github.format00.coinsprice


class Const {
    companion object {
        val PRICES_FRAGMENT_TAG = "PRICES_FRAGMENT_TAG"
        val COINS_FRAGMENT_TAG = "COINS_FRAGMENT_TAG"
        val ALARMS_FRAGMENT_TAG = "ALARMS_FRAGMENT_TAG"
        val BASE_URL = "https://api.coinmarketcap.com"
        val REQUEST_TAG = "REQUEST_TAG"
        val HOME_PREFERENCES_TAG = "HOME_PREFERENCES_TAG"
        val BALANCE_TAG = "BALANCE_TAG"
    }
}
