package com.github.format00.coinsprice.model.entity

class CoinEntity() {
    var _id: Long? = null
    var id : String = ""
    var name : String = ""
    var symbol : String = ""
    var priceUsd : String = ""
    var hourChange : String? = ""
    var dayChange : String? = ""
    var weekChange : String? = ""

    constructor(id : String, name : String, symbol : String, priceUsd : String, hourChange : String?,
                dayChange : String?, weekChange : String?) : this() {
        this.id = id
        this.name = name
        this.symbol = symbol
        this.priceUsd = priceUsd
        this.hourChange = hourChange
        this.dayChange = dayChange
        this.weekChange = weekChange
    }

    override fun toString(): String {
        return "CoinEntity(_id=$_id, id='$id', name='$name', symbol='$symbol', priceUsd='$priceUsd', " +
                "hourChange='$hourChange', dayChange='$dayChange', weekChange='$weekChange')"
    }


}