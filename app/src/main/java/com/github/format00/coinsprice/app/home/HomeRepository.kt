package com.github.format00.coinsprice.app.home


interface HomeRepository {
    fun saveBalance(balance : Double)
    fun retrieveBalance() : Double
}