package com.github.format00.coinsprice.mapper

import com.github.format00.coinsprice.model.Coin
import com.github.format00.coinsprice.model.entity.CoinEntity

class CoinMapper {
    fun map(coin : Coin) : CoinEntity {
        val entity = CoinEntity(coin.id, coin.name, coin.symbol, coin.priceUsd, coin.hourChange, coin.dayChange, coin.weekChange)
        return entity
    }

    fun map(entity: CoinEntity) : Coin {
        val coin = Coin(entity.id, entity.name, entity.symbol, entity.priceUsd, entity.hourChange, entity.dayChange, entity.weekChange)
        return coin
    }
}