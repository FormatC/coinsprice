package com.github.format00.coinsprice.app.coins_screen

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import com.github.format00.coinsprice.R
import com.github.format00.coinsprice.adapter.PricesAdapter
import com.github.format00.coinsprice.app.home.HomeActivity
import com.github.format00.coinsprice.di.component.DaggerPricesComponent
import com.github.format00.coinsprice.di.module.AppModule
import com.github.format00.coinsprice.di.module.DataModule
import com.github.format00.coinsprice.di.module.NetworkModule
import com.github.format00.coinsprice.di.module.PricesModule
import com.github.format00.coinsprice.model.Coin
import com.github.format00.coinsprice.model.Sort
import com.github.format00.coinsprice.model.entity.OrderEntity
import java.text.DecimalFormat
import javax.inject.Inject

class PricesFragment : Fragment(), PricesView {

    @BindView(R.id.prices)
    lateinit var prices: RecyclerView

    @BindView(R.id.progressBar)
    lateinit var progressBar: ProgressBar

    private var pricesAdapter: PricesAdapter? = null
    private var currentData: List<Coin> = ArrayList()
    private var toolbarSpinner: Spinner? = null
    private var isDataLoading = false

    @Inject
    lateinit var presenter: PricesPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerPricesComponent.builder()
                .appModule(AppModule(activity))
                .networkModule(NetworkModule())
                .dataModule(DataModule())
                .pricesModule(PricesModule(this))
                .build()
                .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_prices, container, false)
        ButterKnife.bind(this, view)
        (activity as HomeActivity).showToolbarSpinner()
        val list: MutableList<Coin> = mutableListOf()
        pricesAdapter = PricesAdapter(activity, list, object : PricesAdapter.PriceClickListener {
            override fun onPriceItemClicked(coin: Coin) {
                setupBuyDialog(coin)
            }
        })
        val manager = LinearLayoutManager(activity)

        prices.adapter = pricesAdapter
        prices.layoutManager = manager

        toolbarSpinner = (activity as HomeActivity).getSpinner()
        toolbarSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, spinnerView: View?, position: Int, id: Long) {
                loadData()
            }
        }

        if (currentData.isEmpty() && !isDataLoading) {
            loadData()
        }

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as HomeActivity).hideToolbarSpinner()
    }

    private fun loadData() {
        if (!isDataLoading) {
            isDataLoading = true
            val selectedSort =
                    toolbarSpinner?.selectedItem as String
            val setupSort = setupSort(selectedSort)

            presenter.loadAllPricesSort(setupSort)
        }

    }

    fun setupBuyDialog(coin: Coin) {
        val dialog = AlertDialog.Builder(activity).create()
        val dialogView = LayoutInflater.from(activity).inflate(R.layout.buy_dialog, null, false)
        val coinsValue = dialogView.findViewById(R.id.coinsValue) as EditText?
        val currencySymbol = dialogView.findViewById(R.id.currencySymbol) as TextView?
        val totalPriceValue = dialogView.findViewById(R.id.totalPriceValue) as TextView?
        val currentBalanceValue = dialogView.findViewById(R.id.currentBalanceValue) as TextView?
        val balanceAfterOrderValue = dialogView.findViewById(R.id.balanceAfterOrderValue) as TextView?
        val coinPriceValue = dialogView.findViewById(R.id.coinPriceValue) as TextView?
        val coinPriceLabel = dialogView.findViewById(R.id.coinPriceLabel) as TextView?
        val cancelButton = dialogView.findViewById(R.id.cancelButton) as Button?
        val okButton = dialogView.findViewById(R.id.okButton) as Button?
        val coinPrice = coin.priceUsd.toDouble()
        val currentBalance = presenter.retrieveCurrentBalance()

        currencySymbol?.text = coin.symbol
        currentBalanceValue?.text = "${DecimalFormat("##.##").format(currentBalance)}$"
        balanceAfterOrderValue?.text = "$currentBalance$"
        coinPriceValue?.text = "${DecimalFormat("##.#######").format(coinPrice)}$"
        coinPriceLabel?.text = "${coin.symbol} price:"

        coinsValue?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(value: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    val valueDouble = value?.toString()?.toDouble()
                    val newTotalPrice = valueDouble?.times(coinPrice)
                    val newBalanceAfterOrder = currentBalance.minus(newTotalPrice!!)
                    totalPriceValue?.text = "${DecimalFormat("##.#######").format(newTotalPrice)}$"
                    balanceAfterOrderValue?.text = "${DecimalFormat("##.##").format(newBalanceAfterOrder)}$"
                    val balanceAfterOrderValueColor: Int
                    if (newBalanceAfterOrder >= 0) {
                        okButton?.isClickable = true
                        okButton?.alpha = 1f
                        balanceAfterOrderValueColor = R.color.green
                    } else {
                        okButton?.isClickable = false
                        okButton?.alpha = 0.5f
                        balanceAfterOrderValueColor = R.color.red
                    }
                    balanceAfterOrderValue?.setTextColor(context.resources.getColor(balanceAfterOrderValueColor))
                } catch (e: NumberFormatException) {
                    e.printStackTrace()
                    totalPriceValue?.text = "0.00$"
                }
            }
        })

        cancelButton?.setOnClickListener { dialog.dismiss() }
        okButton?.setOnClickListener {
            try {
                val totalCoinsAmount = coinsValue?.text?.toString()?.toDouble()
                val totalUsdSpent = totalCoinsAmount!! * coin.priceUsd.toDouble()
                val order = OrderEntity(coin.id, coin.name, coin.symbol, coin.priceUsd.toDouble(),
                        totalCoinsAmount, totalUsdSpent)
                presenter.saveOrder(order)
                presenter.updateCurrentBalance(currentBalance - totalUsdSpent)
            } catch (e: NumberFormatException) {
                e.printStackTrace()
            }
            dialog.dismiss()
        }
        dialog.setView(dialogView)
        dialog.show()
    }

    fun setupSort(selectedSort: String?): Sort? {
        return when (selectedSort) {
            getString(R.string.price_ascending) -> Sort.PRICE_ASC
            getString(R.string.price_descending) -> Sort.PRICE_DESC
            getString(R.string.change_24h_ascending) -> Sort.CHANGE_24h_ASC
            getString(R.string.change_24h_descending) -> Sort.CHANGE_24h_DESC
            getString(R.string.change_1w_ascending) -> Sort.CHANGE_1W_ASC
            getString(R.string.change_1w_descending) -> Sort.CHANGE_1W_DESC
            getString(R.string.defaultSort) -> Sort.DEFAULT
            else -> null
        }
    }

    override fun onSuccessLoading(data: List<Coin>) {
        isDataLoading = false
        currentData = ArrayList(data)
        pricesAdapter?.swap(data)
    }

    override fun onLoadingFail(t: Throwable) {
        isDataLoading = false
        Toast.makeText(activity, "Error during data loading", Toast.LENGTH_SHORT).show()
        t.printStackTrace()
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
        prices.visibility = View.VISIBLE
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
        prices.visibility = View.GONE
    }
}