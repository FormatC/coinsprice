package com.github.format00.coinsprice.di.module

import android.content.Context
import com.github.format00.coinsprice.api.CoinMarketCapService
import com.github.format00.coinsprice.app.coins_screen.*
import com.github.format00.coinsprice.di.qualifier.UiThread
import com.github.format00.coinsprice.di.qualifier.WorkingThread
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import nl.qbusict.cupboard.DatabaseCompartment

@Module
class PricesModule(private val view: PricesView) {

    @Provides
    fun providePricesView() = view

    @Provides
    fun providePricesRepository(context: Context, cupboard: DatabaseCompartment): PricesRepository
            = PricesRepositoryImpl(context, cupboard)

    @Provides
    fun providePricesPresenter(service: CoinMarketCapService, view: PricesView, @WorkingThread workingScheduler: Scheduler,
                               @UiThread uiScheduler: Scheduler, repo: PricesRepository): PricesPresenter {
        return PricesPresenterImpl(service, view, workingScheduler, uiScheduler, repo)
    }

}