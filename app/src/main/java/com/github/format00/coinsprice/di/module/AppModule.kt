package com.github.format00.coinsprice.di.module

import android.content.Context
import com.github.format00.coinsprice.di.qualifier.UiThread
import com.github.format00.coinsprice.di.qualifier.WorkingThread
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Module
class AppModule(private val context: Context) {

    @Provides
    fun provideApplicationContext(): Context = context

    @WorkingThread
    @Provides
    fun provideWorkingScheduler() : Scheduler = Schedulers.newThread()

    @UiThread
    @Provides
    fun provideUiScheduler() : Scheduler = AndroidSchedulers.mainThread()

}
