package com.github.format00.coinsprice.app.coins_screen


import com.github.format00.coinsprice.api.CoinMarketCapService
import com.github.format00.coinsprice.model.Coin
import com.github.format00.coinsprice.model.Sort
import com.github.format00.coinsprice.model.entity.OrderEntity
import io.reactivex.Scheduler
import java.util.*

class PricesPresenterImpl(private val service: CoinMarketCapService, private val view: PricesView,
                          private val workingScheduler: Scheduler, private val uiScheduler: Scheduler,
                          private val repository: PricesRepository) : PricesPresenter {
    override fun loadAllPrices() {
        view.showProgress()
        service.getAllPrices()
                .observeOn(uiScheduler)
                .subscribeOn(workingScheduler)
                .subscribe(this::onSuccessLoading, this::onLoadingFail)
    }

    override fun loadPrices(limit: Int) {
        view.showProgress()
        service.getAllPrices(limit)
                .observeOn(uiScheduler)
                .subscribeOn(workingScheduler)
                .subscribe(this::onSuccessLoading, this::onLoadingFail)
    }

    override fun loadAllPricesSort(sort: Sort?) {
        view.showProgress()
        service.getAllPrices()
                .map { sortCurrencies(it, sort) }
                .observeOn(uiScheduler)
                .subscribeOn(workingScheduler)
                .subscribe(this::onSuccessLoading, this::onLoadingFail)
    }

    private fun onSuccessLoading(list: List<Coin>) {
        view.hideProgress()
        view.onSuccessLoading(list)
    }

    private fun onLoadingFail(t: Throwable) {
        view.hideProgress()
        view.onLoadingFail(t)
    }

    override fun retrieveCurrentBalance(): Double = repository.retrieveBalance()

    fun sortCurrencies(list: List<Coin>, sort: Sort?): List<Coin> {
        return when (sort) {
            Sort.PRICE_DESC -> list.sortedByDescending { it.priceUsd.toDouble() }
            Sort.PRICE_ASC -> list.sortedBy { it.priceUsd.toDouble() }
            Sort.CHANGE_1W_ASC -> list.filter { it.weekChange != null }.sortedBy { it.weekChange?.toDouble() }
            Sort.CHANGE_1W_DESC -> list.filter { it.weekChange != null }.sortedByDescending { it.weekChange?.toDouble() }
            Sort.CHANGE_24h_ASC -> list.filter { it.dayChange != null }.sortedBy { it.dayChange?.toDouble() }
            Sort.CHANGE_24h_DESC -> list.filter { it.dayChange != null }.sortedByDescending { it.dayChange?.toDouble() }
            Sort.DEFAULT -> list
            else -> Collections.emptyList()
        }
    }

    override fun saveOrder(order : OrderEntity) {
        repository.saveOrder(order)
    }

    override fun updateCurrentBalance(newValue: Double) {
        repository.updateCurrentBalance(newValue)
    }
}
