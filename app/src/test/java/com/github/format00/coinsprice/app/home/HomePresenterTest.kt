package com.github.format00.coinsprice.app.home

import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class HomePresenterTest {
    @Mock
    lateinit var repository : HomeRepository

    private lateinit var presenter : HomePresenter
    private val testBalance = 1.0

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = HomePresenterImpl(repository)
    }

    @Test
    fun testSaveBalance() {
        presenter.saveBalance(testBalance)
        verify(repository).saveBalance(testBalance)
    }

    @Test
    fun retrieveBalance() {
        presenter.retrieveBalance()
        verify(repository).retrieveBalance()
    }
}
