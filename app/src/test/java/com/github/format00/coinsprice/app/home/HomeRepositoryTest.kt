package com.github.format00.coinsprice.app.home

import android.content.Context
import android.content.SharedPreferences
import com.github.format00.coinsprice.Const
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Matchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations


class HomeRepositoryTest {

    @Mock
    lateinit var context : Context

    @Mock
    lateinit var prefs : SharedPreferences

    private lateinit var repository : HomeRepository

    private val TEST_BALANCE = 1.0

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`(context.getSharedPreferences(Const.HOME_PREFERENCES_TAG, Context.MODE_PRIVATE))
                .thenReturn(prefs)
        repository = HomeRepositoryImpl(context)
    }


    @Test
    fun shouldSaveBalanceToSharedPrefs() {
        val editor = mock(SharedPreferences.Editor::class.java)
        val putEditor = mock(SharedPreferences.Editor::class.java)
        `when`(prefs.edit()).thenReturn(editor)
        `when`(editor.putString(Matchers.anyString(), Matchers.anyString())).thenReturn(putEditor)
        repository.saveBalance(TEST_BALANCE)
        verify(prefs).edit()
        verify(editor).putString(Const.BALANCE_TAG, TEST_BALANCE.toString())
        verify(putEditor).commit()
    }

    @Test
    fun shouldRetrieveBalanceFromPrefs() {
        `when`(prefs.getString(Const.BALANCE_TAG, "0.00")).thenReturn(TEST_BALANCE.toString())
        val retrievedBalance = repository.retrieveBalance()
        assertEquals(retrievedBalance, TEST_BALANCE, 0.01)
    }

}