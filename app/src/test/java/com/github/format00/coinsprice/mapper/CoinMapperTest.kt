package com.github.format00.coinsprice.mapper

import com.github.format00.coinsprice.model.Coin
import com.github.format00.coinsprice.model.entity.CoinEntity
import org.junit.Assert.assertEquals
import org.junit.Test

class CoinMapperTest {
    private val mapper = CoinMapper()

    @Test
    fun shouldCorrectlyMapToCoinEntity() {
        val coin = Coin("monero", "Monero", "xmr", "150", "2", "-3", "15")
        val mapper = mapper.map(coin)
        assertEquals(coin.dayChange, mapper.dayChange)
        assertEquals(coin.hourChange, mapper.hourChange)
        assertEquals(coin.id, mapper.id)
        assertEquals(coin.name, mapper.name)
        assertEquals(coin.priceUsd, mapper.priceUsd)
        assertEquals(coin.symbol, mapper.symbol)
        assertEquals(coin.weekChange, mapper.weekChange)
    }

    @Test
    fun shouldCorrectlyMapToCoin() {
        val entity = CoinEntity("monero", "Monero", "xmr", "150", "2", "-3", "15")
        val mapper = mapper.map(entity)
        assertEquals(entity.dayChange, mapper.dayChange)
        assertEquals(entity.hourChange, mapper.hourChange)
        assertEquals(entity.id, mapper.id)
        assertEquals(entity.name, mapper.name)
        assertEquals(entity.priceUsd, mapper.priceUsd)
        assertEquals(entity.symbol, mapper.symbol)
        assertEquals(entity.weekChange, mapper.weekChange)
    }
}