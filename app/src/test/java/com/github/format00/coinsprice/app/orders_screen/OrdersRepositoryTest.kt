package com.github.format00.coinsprice.app.orders_screen

import android.util.Log
import com.github.format00.coinsprice.api.CoinMarketCapService
import com.github.format00.coinsprice.app.TestData
import com.github.format00.coinsprice.mapper.OrderMapper
import com.github.format00.coinsprice.model.Coin
import com.github.format00.coinsprice.model.entity.OrderEntity
import io.reactivex.Observable
import nl.qbusict.cupboard.DatabaseCompartment
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Matchers.anyString
import org.mockito.Mockito
import org.powermock.api.mockito.PowerMockito
import org.powermock.api.mockito.PowerMockito.`when`
import org.powermock.api.mockito.PowerMockito.mock
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import retrofit2.Call
import retrofit2.Response

@RunWith(PowerMockRunner::class)
@PrepareForTest(Response::class, Coin::class, OrdersRepositoryImpl::class, Log::class)
class OrdersRepositoryTest {


    lateinit var cupboard: DatabaseCompartment
    lateinit var service: CoinMarketCapService
    lateinit var repository: OrdersRepository

    val ordersList = TestData.ordersList
    val coinsList = TestData.coinsList

    @Before
    fun setup() {
        cupboard = mock(DatabaseCompartment::class.java)
        service = mock(CoinMarketCapService::class.java)
        repository = PowerMockito.spy(OrdersRepositoryImpl(cupboard, service, OrderMapper()))
        PowerMockito.mockStatic(Log::class.java)
    }

    @Test
    fun retrieveOrdersShouldReturnCorrectData() {
        val mapper = OrderMapper()
        val queryBuilder = mock(DatabaseCompartment.QueryBuilder::class.java) as DatabaseCompartment.QueryBuilder<OrderEntity>
        `when`(cupboard.query(OrderEntity::class.java)).thenReturn(queryBuilder)
        `when`(queryBuilder.list()).thenReturn(ordersList.map { mapper.map(it) })
        val ordersObservable = repository.retrieveOrders()
        val testObserver = ordersObservable.test()
        testObserver.assertResult(ordersList)
    }

    @Test
    fun retrieveFullOrdersShouldReturnCorrectData() {
        val mockCall = mock(Call::class.java) as Call<List<Coin>>
        val mockResponse = mock(Response::class.java) as Response<List<Coin>>
        Mockito.doReturn(Observable.just(ordersList)).`when`(repository).retrieveOrders()
        `when`(service.getCoinInfoCall(anyString())).thenReturn(mockCall)
        `when`(mockCall.execute()).thenReturn(mockResponse)
        `when`(mockResponse.body()).thenReturn(coinsList)
        val fullOrders = repository.retrieveFullOrders()
        val testObserver = fullOrders.test()
        val values = testObserver.values()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        val value = values[0]
        value.forEach { assertNotNull(it) }
    }

    @Test
    fun deleteOrderShouldReturnTrue() {
        val testOrder = ordersList[0]
        `when`(cupboard.delete(ArgumentMatchers.any(OrderEntity::class.java))).thenReturn(true)
        val testObserver = repository.deleteOrder(testOrder).test()
        testObserver.assertValueCount(1)
        testObserver.assertResult(true)
    }

}