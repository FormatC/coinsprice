package com.github.format00.coinsprice.app

import com.github.format00.coinsprice.model.Coin
import com.github.format00.coinsprice.model.Order

class TestData {
    companion object {
        val coinsList = setupCoins()
        val ordersList = setupOrders()

        private fun setupOrders(): List<Order> {
            val firstOrder = Order("monero", "Monero", "xmr", 150.0, 2.0, 300.0, null)
            val secondOrder = Order("ethereum", "Ethereum", "eth", 200.0, 1.0, 200.0, null)
            val thirdOrder = Order("bitcoin", "Bitcoin", "btc", 2000.0, 10.0, 2000.0, null)
            return listOf(firstOrder, secondOrder, thirdOrder)
        }

        private fun setupCoins(): List<Coin> {
            val coinXmr = Coin("monero", "Monero", "xmr", "150.0", "1.0", "2.0", "3.0")
            val coinEth = Coin("ethereum", "Ethereum", "eth", "150.0", "1.0", "2.0", "3.0")
            val coinBtc = Coin("bitcoin", "Bitcoin", "btc", "1500.0", "10.0", "20.0", "30.0")
            return listOf(coinXmr, coinEth, coinBtc)
        }
    }
}