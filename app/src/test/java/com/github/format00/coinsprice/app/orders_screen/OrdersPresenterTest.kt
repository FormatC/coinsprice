package com.github.format00.coinsprice.app.orders_screen

import com.github.format00.coinsprice.app.TestData
import com.github.format00.coinsprice.model.Order
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations.initMocks
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner


@RunWith(PowerMockRunner::class)
@PrepareForTest(Order::class)
class OrdersPresenterTest {

    @Mock
    lateinit var presenter: OrdersPresenter

    @Mock
    lateinit var repository: OrdersRepository

    @Mock
    lateinit var view: OrdersView

    var workingThread: Scheduler = Schedulers.trampoline()
    lateinit var uiThread: Scheduler

    @Before
    fun setup() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        initMocks(this)
        uiThread = AndroidSchedulers.mainThread()
        presenter = OrdersPresenterImpl(repository, view, uiThread, workingThread)
    }

    @Test
    fun retrieveOrdersShouldReturnCorrectData() {
        val list: List<Order> = ArrayList()
        Mockito.`when`(repository.retrieveFullOrders()).thenReturn(Observable.just(list))
        presenter.loadOrders()
        verify(view).showProgress(false)
        verify(repository).retrieveFullOrders()
        verify(view).hideProgress(false)
        verify(view).onSuccessLoading(list)
    }

    @Test
    fun retrieveOrdersShouldCallOnLoadingFailWhenErrorOccur() {
        Mockito.`when`(repository.retrieveFullOrders()).thenReturn(Observable.error(RuntimeException()))
        presenter.loadOrders()
        verify(view).showProgress(false)
        verify(view).hideProgress(false)
    }


    @Test
    fun shouldDeleteOrderAndRemoveAdapterItem() {
        val mockOrder = PowerMockito.mock(Order::class.java)
        val testPosition = 0
        Mockito.`when`(repository.deleteOrder(mockOrder)).thenReturn(Observable.just(true))
        presenter.deleteOrder(mockOrder, testPosition)
        verify(view).showProgress(true)
        verify(repository).deleteOrder(mockOrder)
        verify(view).hideProgress(true)
        verify(view).removeAdapterItem(testPosition)
    }

    @Test
    fun shouldCorrectlyCalculateTotalSpentValue() {
        val orders = TestData.ordersList
        val totalSpentValue = presenter.retrieveTotalSpentValue(orders)
        assertEquals(totalSpentValue, 2500.0, 0.1)
    }

}