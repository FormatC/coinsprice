package com.github.format00.coinsprice.mapper

import com.github.format00.coinsprice.model.Order
import com.github.format00.coinsprice.model.entity.OrderEntity
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test


class OrdersMapperTest {

    private val mapper  = OrderMapper()

    @Test
    fun shouldCorrectlyMapToOrderEntity() {
        val order = Order("monero", "Monero", "xmr", 150.0, 2.0, 300.0, null)
        val mapped = mapper.map(order)
        assertEquals(mapped.coinId, order.coinId)
        assertEquals(mapped.coinName, order.coinName)
        assertEquals(mapped.coinPrice, order.coinPrice, 0.01)
        assertEquals(mapped.totalCoinsAmount, order.totalCoinsAmount, 0.01)
        assertEquals(mapped.totalUsdSpent, order.totalUsdSpent, 0.01)
        assertEquals(mapped.coinSymbol, order.coinSymbol)
    }

    @Test
    fun shouldCorrectlyMapToOrder() {
        val entity = OrderEntity("monero", "Monero", "xmr", 150.0, 2.0, 300.0)
        val mapped = mapper.map(entity)
        assertEquals(mapped.coinId, entity.coinId)
        assertEquals(mapped.coinName, entity.coinName)
        assertEquals(mapped.coinPrice, entity.coinPrice, 0.01)
        assertEquals(mapped.totalCoinsAmount, entity.totalCoinsAmount, 0.01)
        assertEquals(mapped.totalUsdSpent, entity.totalUsdSpent, 0.01)
        assertEquals(mapped.coinSymbol, entity.coinSymbol)
        assertNull(mapped.coin)
    }
}