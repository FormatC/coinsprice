package com.github.format00.coinsprice.mapper

import com.github.format00.coinsprice.model.Alarm
import com.github.format00.coinsprice.model.entity.AlarmEntity
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test



class AlarmMapperTest {
    private val mapper = AlarmMapper()

    @Test
    fun shouldCorrectlyMapToAlarmEntity() {
        val alarm = Alarm("Monero", "monero", 20.0, 30.0, 50.0, 60.0, null)
        val mapped = mapper.map(alarm)
        assertEquals(alarm.alarmNegative, mapped.alarmNegative, 0.01)
        assertEquals(alarm.alarmPositive, mapped.alarmPositive, 0.01)
        assertEquals(alarm.sellNegative, mapped.sellNegative, 0.01)
        assertEquals(alarm.sellPositive, mapped.sellPositive, 0.01)
        assertEquals(alarm.coinId, mapped.coinId)
        assertEquals(alarm.coinName, mapped.coinName)
        assertNull(alarm.coin)
    }

    @Test
    fun shouldCorrectlyMapToAlarm() {
        val entity = AlarmEntity("Monero", "monero", 20.0, 30.0, 50.0, 60.0)
        val mapped = mapper.map(entity)
        assertEquals(entity.alarmNegative, mapped.alarmNegative, 0.01)
        assertEquals(entity.alarmPositive, mapped.alarmPositive, 0.01)
        assertEquals(entity.sellNegative, mapped.sellNegative, 0.01)
        assertEquals(entity.sellPositive, mapped.sellPositive, 0.01)
        assertEquals(entity.coinId, mapped.coinId)
        assertEquals(entity.coinName, mapped.coinName)
    }
}